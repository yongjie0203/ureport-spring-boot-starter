package com.syyai.spring.boot.ureport;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bstek.ureport.console.UReportServlet;

@Configuration
public class UReportBeanRegisration {
	
	
	static {
		System.out.println("load class :"+UReportBeanRegisration.class.getCanonicalName());
	}
	
	@Bean
    public ServletRegistrationBean<UReportServlet> registerUReportServlet(){
        return new ServletRegistrationBean<UReportServlet>(new UReportServlet(),"/ureport/*");
    }
	
	
	
	
	
}